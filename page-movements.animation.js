(function () {
    'use strict';

    function page_movement($timeout, $rootScope) {

        var pageBody = $('body');
        pageBody.addClass('br__movements');

        return {
            enter: function(element, done) {

                var transType = angular.element(element).attr('data-move-type') !== undefined ? angular.element(element).attr('data-move-type') : 'br__move-vert';
                pageBody.addClass(transType);

                if ($rootScope.enableAnim) {
                    element.addClass('hide__at__first');

                    $timeout(function () {

                        /*$('html, body').animate({
                            scrollTop: 0
                        }, 0);*/

                        element.addClass('br__in');
                    }, 250);

                    $timeout(function () {
                        element.removeClass('hide__at__first');
                    }, 500);
                }

                $timeout(function () {
                    element.removeClass('br_in');
                    done();
                }, 250);
            },
            leave: function(element, done) {

                if ($rootScope.enableAnim) {
                    element.addClass('br__out');
                }

                $timeout(function () {
                    element.removeClass('br_out');
                    done();
                }, 250);
            }
        }

    }

    page_movement.inject = ['$timeout', '$rootScope'];

    angular
        .module('bravoureAngularApp')
        .animation('.br__movement', page_movement);

})();


# Element animations

https://github.com/rpocklin/angular-scroll-animate

# Transitions
The transitions animation is provided by an overlay that animates in the way you want over the visible content.
You can add a option for a preloader icon that appears when the next view is loading.


## How to
  * Add the class **br__trans** to the ui-view element
  * Add this attribute to the ui-view element: **data-trans-type="STRING"** (transition type)
  * Add this attribute to the ui-view element: **data-anim-speed="INT"** (animation speed in ms, 0 - 1000)
  * Add this attribute to the ui-view element: **data-trans-icon="BOOLEAN"** (true or false)

## Transition types
Add one of the classes below:

### Slide
  * Slide from top: **br__trans-slide-top**
  * Slide from right: **br__trans-slide-right**
  
### Rotation
  * Rotate X axis from center: **br__trans-rotate-x**
  * Rotate Y axis from center: **br__trans-rotate-y**
  
### Scale
  * Scale circle from center: **br__trans-scale-circle**
  * Scale square from center: **br__trans-scale-square**
  
### Flip
  * Flip from top: **br__trans-flip-top**


# Movements

The movement animation triggers the views itself to move them to a direction based on a class set on the ui-view element.

## How to
  * Add the class **br__movement** to the ui-view element
  * Add this attribute to the ui-view element: **data-move-type="STRING"** (movement type)

## Movement types
Add one of the classes below:

### Slide
  * Slide from left to right: **br__move-vert**
  * Rotate X axis from center: **br__move-hor**


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-animations": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install

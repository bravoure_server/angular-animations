(function () {

    'use strict';


    function page_transition ($rootScope, $timeout, $window, cfpLoadingBar) {

        // TODO: Check functionality with throttling on low speed. The animations happens twice!!!!

        var transMaskPlaced = false,
            pageBody = $ ('body');

        return {
            enter: function (element, done) {

                if (element.hasClass ('br__trans')) {

                    var sync = $rootScope.$eval (angular.element (element).attr ('data-anim-sync')) !== undefined ? $rootScope.$eval (angular.element (element).attr ('data-anim-sync')) : false,
                        speed = angular.element (element).attr ('data-anim-speed') !== undefined ? $rootScope.$eval (angular.element (element).attr ('data-anim-speed')) : 1000,
                        inSpeed = angular.element (element).attr ('data-anim-in-speed') !== undefined ? $rootScope.$eval (angular.element (element).attr ('data-anim-in-speed')) : speed,
                        outSpeed = angular.element (element).attr ('data-anim-out-speed') !== undefined ? $rootScope.$eval (angular.element (element).attr ('data-anim-out-speed')) : speed,
                        transType = angular.element (element).attr ('data-trans-type') !== undefined ? angular.element (element).attr ('data-trans-type') : 'br__trans-slide-top',
                        hasTransIcon = angular.element (element).attr ('data-trans-icon') !== undefined ? $rootScope.$eval (angular.element (element).attr ('data-trans-icon')) : false;

                    // Dynamically prepend the animating mask to the body of the page
                    if (transMaskPlaced == false) {

                        var transMask = angular.element ("<div class='bravoure__trans-mask-container'><div class='bravoure__trans-mask " + transType + "'></div></div>");
                        pageBody.prepend (transMask);
                        transMaskPlaced = true;

                        // If we want a loading icon when the mask is visible
                        if (hasTransIcon == true) {
                            var transIcon = angular.element ("<div class='bravoure__trans-icon'></div>");
                            transIcon.addClass (transType);
                            pageBody.prepend (transIcon);
                        }
                    }

                    try {

                        var observer = new MutationObserver (function (mutations) {
                            observer.disconnect ();

                            $window.requestAnimationFrame (function () {
                                $timeout (done, sync ? 0 : outSpeed);
                            });
                        });

                        observer.observe (element[0], {
                            attributes: true,
                            childList: false,
                            characterData: false
                        });

                    } catch (e) {
                        $timeout (done, Math.max (100, sync ? 0 : outSpeed));
                    }

                    angular.element (element).addClass ('anim-in-setup');
                    pageBody.addClass ('anim-in');
                    pageBody.addClass ('no-anim');


                    $rootScope.$on ('cfpLoadingBar:completed', function () {

                        $timeout (function () {
                            $rootScope.$broadcast ('animEnd', element, inSpeed);
                            pageBody.addClass ('anim-out');
                            pageBody.removeClass ('anim-in');
                            pageBody.removeClass ('no-anim');
                            $timeout (function () {
                                pageBody.removeClass ('anim-out');
                            }, speed);
                        }, inSpeed);

                    });

                    return function (cancelled) {

                        angular.element (element).removeClass ('anim-in-setup');

                        $rootScope.$on ('cfpLoadingBar:loaded', function () {
                            angular.element (element).removeClass ('anim-in-setup');
                        });

                    };

                }
            },
            leave: function (element, done) {

                if (element.hasClass ('br__trans')) {

                    var speed = angular.element (element).attr ('data-anim-speed') !== undefined ? $rootScope.$eval (angular.element (element).attr ('data-anim-speed')) : 1000,
                        outSpeed = angular.element (element).attr ('data-anim-out-speed') !== undefined ? $rootScope.$eval (angular.element (element).attr ('data-anim-out-speed')) : speed;

                    $rootScope.$broadcast ('animStart', element, outSpeed);

                    try {

                        var observer = new MutationObserver (function (mutations) {
                            observer.disconnect ();

                            $window.requestAnimationFrame (function () {
                                angular.element (element).removeClass ('anim-out-setup');
                                $timeout (done, outSpeed);
                            });
                        });

                        observer.observe (element[0], {
                            attributes: true,
                            childList: false,
                            characterData: false
                        });

                    } catch (e) {
                        angular.element (element).removeClass ('anim-out-setup');
                        $timeout (done, Math.max (100, outSpeed));
                    }

                    angular.element (element).addClass ('anim-out-setup');
                }
            }
        };
    }

    page_transition.inject = ['$rootScope', '$timeout', '$window', 'cfpLoadingBar'];

    angular
        .module ('bravoureAngularApp')
        .animation ('.br__trans', page_transition);

}) ();

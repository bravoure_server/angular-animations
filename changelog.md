## Angular Animations / Bravoure component

These components are used to set the animations when scrolling in the page.
 
### **Versions:**

1.0.1   - Fixed bug in initial commit
1.0     - Initial version

---------------------
